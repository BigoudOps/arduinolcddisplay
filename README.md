# ArduinoLcdDisplay
 
 Getting CPU & memory usage on 16X2 lcd with python3 and arduino

## Circuit

     LCD RS pin to digital pin 12
 
     LCD Enable pin to digital pin 11
 
     LCD D4 pin to digital pin 5
 
     LCD D5 pin to digital pin 4
 
     LCD D6 pin to digital pin 3
 
     LCD D7 pin to digital pin 2


> Additionally, wire a 10k pot to +5V and GND, with it's wiper (output) to LCD screens VO pin (pin3). A 220 ohm resistor is used to power the backlight of the display, usually on pin 15 and 16 of the LCD connector
 
🛠 first install if it is not already pySerial 

Secondly make sure to verify your device name with the command: python -m serial.tools.list_ports -v
if you have desc: USB2.0-Serial, you have nothing else to do. Otherwise copy what is written and replace it at line 23 at testcpu.py 

r = re.compile (r 'YourDescHere*') 